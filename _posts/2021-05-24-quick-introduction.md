---
layout: single
title:  "Quick introduction & GitLab bug huntings!"
date:   2021-05-24 17:54:31 +0700
categories: introductions
---

Just a quick introduction for all the things will be written in this blog.

I will be posting all of my write-ups for security bugs I have found while doing bug bounty, Some of them are, I think, very interesting to sec geeks.

GitLab is undeniably my most favourite bugbounty program with well-structured products, nice triagers and security staffs.
The code written in GitLab's product like `gitlab-workhorse` or `gitlab-rails` are so well-factored to the point where it becomes so plesant to my eyes just by reading it :P

You are probably interested in some of my GitLab bugs, I will soon be updating write-ups along side with disclosed reports on H1, the list goes like:

- [GitLab Arbitrary File Read & Write through Kroki - CVE-2021-22203](/blog/writeups/CVE-2021-22203-gitlab-arbitrary-file-read-write-through-kroki)
- **TBE**

I planned to expand my bug bounty journey not just on **GitLab** but also another products such as **Github**, **Atlassian's products**, etc. Hopefully I have chances to publish write-ups.

Stay tune for more!